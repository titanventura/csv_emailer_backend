import csv
import logging
import os

from django.contrib import messages, auth
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from csv_emailer import settings
from main.models import EmailTask, EmailJob, EmailAttachment
from main.tasks import execute_jobs

import threading


@login_required(login_url='/login')
def home(request):
    return render(request, 'index.html')


def login(request):
    if request.method == 'POST':
        users = User.objects.filter(username=request.POST['username'])
        if users:
            user = users[0]
            auth_user = auth.authenticate(request, username=user.username, password=request.POST['password'])
            if auth_user:
                auth.login(request, user)
                return redirect("email-task-create")
            else:
                messages.error(request, 'Invalid username or password.')
                return render(request, 'login.html')
        else:
            messages.error(request, 'Invalid username or password.')
            return redirect(login)

    return render(request, 'login.html')


class JobThread(threading.Thread):
    def __init__(self, task):
        self.task = task
        threading.Thread.__init__(self)

    def run(self):
        execute_jobs(self.task.jobs.all())


def csv_read(request, id):
    if request.method == 'POST':
        task = EmailTask.objects.get(pk=id)
        print(settings.BASE_DIR)
        file = os.path.join(settings.BASE_DIR, 'uploads', task.csv_file.name)
        print(file)
        with open(file, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            print(csv_reader)
            for record_row in csv_reader:
                if record_row['attachment'] != 'None':
                    (receiver, subject, message, cc, bcc, attachment) = record_row['receiver'], record_row['subject'], \
                                                                    record_row['message'], record_row['cc'], \
                                                                    record_row['bcc'], \
                                                                    'uploads/attachment/' + str(task.id) +record_row[
                                                                        'attachment']
                else:
                    (receiver, subject, message, cc, bcc, attachment) = record_row['receiver'], record_row['subject'], \
                                                                        record_row['message'], record_row['cc'], \
                                                                        record_row['bcc'],record_row['attachment']
                try:
                    EmailJob.objects.create(receiver=receiver, subject=subject, message=message, cc=cc, bcc=bcc,
                                            attachment=attachment, task=task)
                    task.total_jobs += 1
                except Exception as e:
                    logging.error(str(e))
        task.save()
        JobThread(task).start()
        return redirect("email-task-create")


def create_email_task_view(request):
    if request.method == "POST":
        try:
            task_name = request.POST.get("task_name", None)
            csv_file = request.FILES.get("csv_file", None)
            print(csv_file, "is the csv_file")
            if None in [task_name, csv_file]:
                messages.error(request, "Task Name and CSV file should be provided.One of those is missing")
                return redirect("email-task-create")

            task = EmailTask.objects.create(name=task_name, csv_file=csv_file)

            all_files = request.FILES.getlist("attachments", None)
            if all_files is None:
                messages.error(request, "Files should be provided")
                return redirect("email-task-create")
            for file in all_files:
                file.name = str(task.id) + file.name
                print(file.name)
                EmailAttachment.objects.create(task=task, file=file)

            messages.success(request, "All files uploaded..")
            return csv_read(request,task.id)
        except Exception as e:
            print(e)
            messages.error(request, f"Exception occured {e}")
            return redirect("email-task-create")

    elif request.method == "GET":
        return render(request, "email_task_create.html")


@login_required
def logout_view(request):
    logout(request)
    return redirect("login-view")


@login_required
def email_task_list_view(request):
    if request.method == "GET":
        tasks = EmailTask.objects.all().order_by("-created_at")
        return render(request, "email_task_list.html", {"email_tasks": tasks})
