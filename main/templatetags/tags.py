from django import template
from django.shortcuts import get_object_or_404

from main.models import EmailTask, EmailJob

register = template.Library()

@register.filter(name='get_successful_jobs')
def successful_jobs(id):
    task = get_object_or_404(EmailTask,pk=id)
    return task.jobs.filter(status=EmailJob.SUCCESS).count()