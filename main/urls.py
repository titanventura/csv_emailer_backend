from django.urls import path
from .views import *

urlpatterns = [
    path("email_task_create/", create_email_task_view, name='email-task-create'),
    path("email_tasks/", email_task_list_view, name='email-task-list')
]
