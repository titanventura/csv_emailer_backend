import os
from uuid import uuid4

from django.db import models
from django.utils import timezone as tz


# Create your models here.
class EmailTask(models.Model):
    def __str__(self):
        return self.name

    name = models.CharField(max_length=30)
    created_at = models.DateTimeField(default=tz.now)
    completed_at = models.DateTimeField(null=True, blank=True)
    total_jobs = models.IntegerField(default=0)
    completed_jobs = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)
    csv_file = models.FileField(upload_to='csv/')


class EmailAttachment(models.Model):
    task = models.ForeignKey(EmailTask, on_delete=models.CASCADE, related_name='attachments')
    file = models.FileField(upload_to='attachment/')


class EmailJob(models.Model):
    def __str__(self):
        return self.receiver

    INITIATED = 0
    SUCCESS = 1
    FAILED = 2
    receiver = models.EmailField()
    cc = models.TextField(null=True, blank=True)
    bcc = models.TextField(null=True, blank=True)
    message = models.TextField(null=True, blank=True)
    subject = models.CharField(max_length=50)
    attachment = models.CharField(max_length=20, null=True)
    scheduled_at = models.DateTimeField(default=tz.now)
    completed_at = models.DateTimeField(null=True, blank=True)
    task = models.ForeignKey(EmailTask, on_delete=models.CASCADE, related_name='jobs')
    status = models.IntegerField(choices=((INITIATED, 'INITIATED'), (SUCCESS, 'SUCCESS'), (FAILED, 'Failed')),
                                 default=INITIATED)
