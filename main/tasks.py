import logging
from django.utils import timezone as tz
import threading
import time
import concurrent.futures
from django.core.mail import EmailMultiAlternatives

from .models import EmailJob

class EmailThread(threading.Thread):
    def __init__(self, job, fail_silently):
        self.job = job
        self.fail_silently = fail_silently
        threading.Thread.__init__(self)

    def run(self):
        job = self.job
        msg = EmailMultiAlternatives(job.subject, job.message, 'ADARSH A<adarsh.18ec@kct.ac.in>', [job.receiver])
        if job.attachment:
            msg.attach_file(job.attachment)
        count = 0
        while count < 5:
            try:
                msg.send(self.fail_silently)
                job.status = EmailJob.SUCCESS
                job.completed_at = tz.now()
                task = job.task
                task.completed_jobs += 1
                if task.completed_jobs == task.total_jobs:
                    task.completed_at = tz.now()
                    task.completed = True
                task.save()
                job.save()
                break
            except Exception as e:
                if count == 4:
                    logging.error(str(e))
                    job.status = EmailJob.FAILED
                    job.completed_at = tz.now()
                    task = job.task
                    task.completed_jobs += 1
                    if task.completed_jobs == task.total_jobs:
                        task.completed_at = tz.now()
                        task.completed = True
                    task.save()
                    job.save()
                time.sleep(pow(2,count+1))
                count += 1

def execute_job(job):
    msg = EmailMultiAlternatives(job.subject, job.message, 'ADARSH A<adarsh.18ec@kct.ac.in>', [job.receiver])
    if job.attachment != 'None':
        msg.attach_file(job.attachment)
    if job.cc != 'None':
        cc = job.cc.split(':')
        msg.cc = cc
    if job.bcc != 'None':
        bcc = job.bcc.split(':')
        msg.bcc = bcc
    count = 0
    while count < 5:
        try:
            msg.send(False)
            job.status = EmailJob.SUCCESS
            job.completed_at = tz.now()
            task = job.task
            task.completed_jobs += 1
            if task.completed_jobs == task.total_jobs:
                task.completed_at = tz.now()
                task.completed = True
            task.save()
            job.save()
            break
        except Exception as e:
            if count == 4:
                print(str(e))
                logging.error(str(e))
                job.status = EmailJob.FAILED
                job.completed_at = tz.now()
                task = job.task
                # task.completed_jobs += 1
                if task.completed_jobs == task.total_jobs:
                    task.completed_at = tz.now()
                    task.completed = True
                task.save()
                job.save()
            time.sleep(pow(2, count + 1))
            count += 1

def execute_jobs(jobs,fail_silently=False,*args, **kwargs):
    task = jobs[0].task
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        # Start the load operations and mark each future with its URL
        future_to_url = {executor.submit(execute_job, job): job for job in jobs}
        # for future in concurrent.futures.as_completed(future_to_url):
        #     url = future_to_url[future]
        #     try:
        #         data = future.result()
        #     except Exception as exc:
        #         print('%r generated an exception: %s' % (url, exc))
        #     else:
        #         print('%r page is %d bytes' % (url, len(data)))
    failed = task.jobs.filter(status=EmailJob.FAILED)
    retry_failed_jobs(failed)


def retry_failed_jobs(jobs):
    execute_jobs(jobs)