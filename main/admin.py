from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(EmailJob)
admin.site.register(EmailAttachment)
admin.site.register(EmailTask)